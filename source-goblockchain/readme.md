# go blockchain

> go语言进行区块链基础编程 

## P1 实现基本区块链数据结构

### 操作步骤
#### 1 使用包管理器
* `go mod init your_module_name`  用于将当前文件夹使用包管理器进行管理，自动创建`go.mod`文件，将来包依赖都在该文件夹
* 当引用外部模块时，可使用`go mod tidy`命令，也可使用`go get "moduel-name"`
#### 2 创建blockchain子包
包括
* block.go  实现Block区块数据结构
* blockchain.go  实现Blockchain数组
#### 3 创建main文件
main进行区块链结构测试

### 自检
1. 如何创建go模块
2. 如何创建子模块
3. 子模块如何引用
4. 如何引用外部模块

## P2 
### 预习
* 如何创建类
  * 如何设置类的属性
  * 如何设置类的方法
  * 为什么设置类的String方法
  * 如何设置私有、公共属性与方法
* 数组与切片有什么区别，如何使用
* 如何将多个字节流连接成一个
* 如何对数据求hash
* 时间在计算机中如何表示，Go语言中如何使用时间、时间戳、字符串来表示时间


### 学习参考
* GO effetive 知识点
  *  https://go.dev/doc/effective_go  , 
  *  https://golang.google.cn/doc/effective_go 
  *  https://learnku.com/docs/effective-go/2020/introduction/6236
* GO 例子学go语言 https://gobyexample.com/ 
* 包管理 https://golang.google.cn/doc/tutorial/create-module
