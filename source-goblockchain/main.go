package main

import (
	"fmt"
	"time"

	"gitee.com/pkucg/bct/blockchain"
)

func blockchainRun() {
	chain := blockchain.InitBlockChain()

	chain.AddBlock("First Block after Genesis")
	time.Sleep(time.Second * 2)
	chain.AddBlock("Second Block after Genesis")
	time.Sleep(time.Second * 4)
	chain.AddBlock("Third Block after Genesis")

	for _, block := range chain.Blocks {
		fmt.Printf("%s", block)
	}
}
func main() {
	blockchainRun()
	// timetest()
}
func timetest() {
	timestamp_second := time.Now().Unix()
	timestamp_nano := time.Now().UnixNano()
	t := time.Unix(timestamp_second, 0)
	t1 := time.Unix(0, timestamp_nano)
	fmt.Printf("%v %v", t, t1)
}
