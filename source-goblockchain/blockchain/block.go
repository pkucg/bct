package blockchain

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"time"
)

type Block struct {
	Timestamp int64
	Hash      []byte
	Data      []byte
	PrevHash  []byte
}

func (b *Block) deriveHash() {
	info := bytes.Join([][]byte{b.Data, b.PrevHash}, []byte{})
	hash := sha256.Sum256(info)
	b.Hash = hash[:]
}

func CreateBlock(data string, prevHash []byte) *Block {
	block := &Block{time.Now().UnixNano(), []byte{}, []byte(data), prevHash}
	block.deriveHash()
	return block
}

func Genesis() *Block {
	return CreateBlock("Genesis", []byte{})
}

func (b *Block) String() string {
	str_fmt := "Simple Block of Hash:%x\n" +
		"  Timestamp:%v\n" +
		"  Data:%s\n" +
		"  PreHash:%x\n"
	return fmt.Sprintf(str_fmt, b.Hash, time.Unix(0, b.Timestamp), b.Data, b.PrevHash)
}
