package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
)

func main() {
	fmt.Println("hello")
	buff := new(bytes.Buffer)
	data := []int64{10, 20}
	err := binary.Write(buff, binary.BigEndian, data)
	if err != nil {
		log.Panic(err)
	}
	b := buff.Bytes()
	fmt.Printf("%x", b)

}
