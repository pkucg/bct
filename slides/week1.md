# 区块链生态与技术栈 

## 1 讲什么 
* 区块链1.0核心技术（go模拟仿真）
* 区块链2.0智能合约（以太坊）

问：
* 有哪些基础？
* 本学期要学的有什么特点?
* 后续课程会学哪些？

## 2 如何学
* 动手
* 动脑
* 找技巧

## 3 如何考试
* 考试+项目

## 4 参考材料
* 比特币官网开发文档 https://developer.bitcoin.org/devguide/
* 一个go实现的例子 https://github.com/Jeiwan/blockchain_go
  *  https://jeiwan.net/
  *  https://github.com/liuchengxu/blockchain-tutorial
*  以太坊官方文档 https://ethereum.org/en/developers/docs/
   *  https://ethereum.org/zh/developers/docs/